import React, { Component } from 'react'
import classes from './App.css'
import {  connect } from 'react-redux'
import * as actions from './actions'
import SearchBar from './components/searchBar/searchBar'

class App extends Component {
  render() {
    const {temperature} = this.props
    return (
      <div>
        <div className={classes.searchContainer}>
          <SearchBar fetchTemperature ={this.props.onFetchTemperatureHandler}/>
        </div>
        {temperature? <span>Temperature is : {temperature} F</span> : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(state.temperature)
  return {
    temperature: state.temperature.data ? state.temperature.data.query.results.channel.item.condition.temp: null
  }
}


const mapDispatchToProps = dispatch => {
  return {
    onFetchTemperatureHandler: (e) => dispatch(actions.onFetchTemperature(e))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
