import * as actionTypes from './actionTypes';
import axios from 'axios';

// on searching showing shimmers
export const onFetchTemperature  = e => async dispatch => {
    if(e.keyCode === 13){
        dispatch({type:actionTypes.LOADING_DATA, payload:{loader: true}})
        const res = await axios.get(`https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text%3D"${e.target.value}%2C ak")&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys`)
        // if its an unknown place search again
        if(res.data.query.results.channel.item){
            dispatch({type: actionTypes.FETCH_TEMPERATURE, payload: {data: res.data, loader:false}})
        }else{
            dispatch({type:actionTypes.PLACE_NOT_FOUND, payload:{data:{}, loader: false}})
        }
    }
}