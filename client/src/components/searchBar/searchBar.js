import React from 'react';
import TextField from '@material-ui/core/TextField';

const searchBar = props => {
    return (
        <TextField 
            fullWidth
            placeholder ="Search for a Place + Hit Enter"
            onKeyUp = {props.fetchTemperature}
            />
    )
}

export default searchBar