import * as actionTypes from '../actions/actionTypes'

const defaultState ={
    loader: false,
    data: null
}

export default (state=defaultState,action) => {
    switch(action.type){
        case actionTypes.LOADING_DATA: 
            return{
                ...state,
                loader: action.payload.loader
            }
        case actionTypes.FETCH_TEMPERATURE: 
            return {
                ...state,
                loader: action.payload.loader,
                data: action.payload.data                
            }
        case actionTypes.PLACE_NOT_FOUND: 
            return{
                ...state,
                loader: action.payload.loader,
                data: action.payload.data
            }
        default: {
            return {
                ...state
            }
        }
    }
}